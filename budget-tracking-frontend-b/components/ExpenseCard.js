import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function ExpenseCard({ expenseProp }) {
    const {expenseName, expenseAmount} = expenseProp;



    return (
        <Card>
            <Card.Body>
                <Card.Title>{expenseName}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Description:</span>
                    <br />
                    PHP {expenseAmount}

                    
                </Card.Text>


                <Button 
                    className="bg-primary" 
                >
                    Delete
                    </Button>
            </Card.Body>
        </Card>
    )
}

ExpenseCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    expenseCard: PropTypes.shape({
        expenseName: PropTypes.string.isRequired,
        expenseAmount: PropTypes.number.isRequired
        // date: PropTypes.number.isRequired
    })
}