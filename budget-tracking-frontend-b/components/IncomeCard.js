import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function IncomeCard({ incomeProp }) {
    const {incomeName, incomeAmount} = incomeProp;



    return (
        <Card>
            <Card.Body>
                <Card.Title>{incomeName}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Description:</span>
                    <br />
                    PHP {incomeAmount}

                    
                </Card.Text>


                <Button 
                    className="bg-primary" 
                >
                    Delete
                    </Button>
            </Card.Body>
        </Card>
    )
}

IncomeCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    incomeProp: PropTypes.shape({
        incomeName: PropTypes.string.isRequired,
        incomeAmount: PropTypes.number.isRequired
        // date: PropTypes.number.isRequired
    })
}