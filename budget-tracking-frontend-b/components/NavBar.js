import { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'

import UserContext from '../UserContext'

export default function NavBar() {
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    
                    {(user.id !== null)
                        ? <React.Fragment>
                            <Link href="/records">
                                <a className="nav-link" role="button">Records</a>
                            </Link>
                            <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>
                            <Link href="/addCategories">
                                <a className="nav-link" role="button">Add Categories</a>
                            </Link>
                            <Link href="/userProfile">
                                <a className="nav-link" role="button">Profile</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </React.Fragment>
                        : <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}


                            // <Link href="/travel">
                            //     <a className="nav-link" role="button">Travel</a>
                            // </Link>
                            // <Link href="/insights">
                            //     <a className="nav-link" role="button">Insights</a>
                            // </Link>
                            // <Link href="/history">
                            //     <a className="nav-link" role="button">History</a>
                            // </Link>
