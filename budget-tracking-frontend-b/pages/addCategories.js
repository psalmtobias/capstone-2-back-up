import { useState } from 'react';
// import Form from 'react-bootstrap/Form';
// import Button from 'react-bootstrap/Button';
import { Form, Button, Dropdown, Row, Col, FormControl, ButtonGroup, Radio } from 'react-bootstrap';
import Router from 'next/router';
import InputGroup from 'react-bootstrap/InputGroup'


export default function addCategories() {
    //declare form input states
    const [categoryName, setCategoryName] = useState('');
    const [isExpense, setIsExpense] = useState(false);

    //function for processing creation of a new course
    function addCatagory(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/courses', {
            method:'POST',
            headers:{
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                isExpense: isExpense,
            })
        })
        .then(res => {
                return res.json()
            })
            .then(data => {
                if(data === true) {
                    Router.push('/addCategories')
                } else {
                    Router.push('/errors/1')
                }
            })
    }

    return (
        <Form onSubmit={(e) => addCatagory(e)}>
            <Row>
                <Col>
                    <Form.Group controlId="categoryName">
                        <Form.Label>Category Name:</Form.Label>
                        <Form.Control 
                            type="text"
                            placeholder="Enter course name"
                            value={categoryName}
                            onChange={e => setCategoryName(e.target.value)}
                            required
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button className="bg-primary" size="lg" block type="submit">
                        Add Income Category
                    </Button>
                </Col>
                <Col>
                    <Button className="bg-primary" size="lg" block type="submit">
                        Add Expense Category
                    </Button>
                </Col>
            </Row>
            
            


            
        </Form>
    )
}





            // <Form.Group controlId="description">
            //     <Form.Label>Course Description:</Form.Label>
            //     <Form.Control
            //         as="textarea"
            //         rows="3"
            //         placeholder="Enter course description"
            //         value={description}
            //         onChange={e => setDescription(e.target.value)}
            //         required
            //     />
            // </Form.Group>
