import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights() {

	const [distances, setDistances] = useState([])
	const [amounts, setAmounts] = useState([])
	const [durations, setDurations] = useState([])
 	
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.travels.length > 0 ){
				let monthlyDistance = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDurations = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyAmount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					// January = 0, February = 1 ......
					const index = moment(travel.date).month()

					monthlyDistance[index] += (travel.distance/1000)
					monthlyAmount[index] += (travel.charge.amount)
					monthlyDuration[index] += (parseInt(travel.duration)/60)
				})

				setDistances(monthlyDistance)
				setAmounts(monthlyAmount)
				setDurations(monthlyDuration)
				console.log(monthlyDistance)
			}
		})
	}, [])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.travels.length > 0 ){
				let monthlyDurations = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					// January = 0, February = 1 ......
					const index = moment(travel.duration)

				})

				setDistances(monthlyDurations)
				console.log(monthlyDurations)
			}
		})
	}, [])



	return (
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={distances} label="Monthly total in kilometers" />
			</Tab>
			<Tab eventKey="durations" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={durations} label="Monthly total in minutes" />
			</Tab>
			<Tab eventKey="amounts" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={amounts} label="Monthly total in Philippine Peso" />
			</Tab>
		</Tabs>
	)
}