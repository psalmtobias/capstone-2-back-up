import Head from 'next/head'
import { Form, Button, Row, Col, Container } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import Router from 'next/router'
import IncomeCard from '../components/IncomeCard'
import ExpenseCard from '../components/ExpenseCard'





export default function records( data ) {

	const [gettingIeName, setGettingIeName] = useState('')
	const [gettingIeAmount, setGettingIeAmount] = useState('')

	const [ieName, setIeName] = useState('')
	const [ieAmount, setIeAmount] = useState(0)

	const [incomeName, setIncomeName] = useState('')
	const [incomeAmount, setIncomeAmount] = useState(0)
	const [incomeNameGet, setIncomeNameGet] = useState([])
	const [incomeAmountGet, setIncomeAmountGet] = useState([])

	const [expenseName, setExpenseName] = useState('')
	const [expenseAmount, setExpenseAmount] = useState(0)
	const [expenseNameGet, setExpenseNameGet] = useState([])
	const [expenseAmountGet, setExpenseAmountGet] = useState([])


	const [results, setResults] = useState([])
	
	

	function addIncome(e) {
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addIncome`, {
        	method: 'POST',
        	headers: {
        		'Authorization': `Bearer ${localStorage.getItem('token')}`,
            	'Content-Type': 'application/json'
        	},
        	body: JSON.stringify({
            	incomeName: incomeName,
            	incomeAmount: incomeAmount
	        })
    	})
    	.then(res => res.json())
    	.then(data => {
    		if(data === true) {
    			Router.push('/records')
    		} else {
    			Router.push('/error')
    		}
    	})

		
	}

	function addExpense(d) {
		d.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addExpense`, {
        	method: 'POST',
        	headers: {
        		'Authorization': `Bearer ${localStorage.getItem('token')}`,
            	'Content-Type': 'application/json'
        	},
        	body: JSON.stringify({
            	expenseName: expenseName,
            	expenseAmount: expenseAmount
	        })
    	})
    	.then(res => res.json())
    	.then(data => {
    		if(data === true) {
    			Router.push('/records')
    			
    		} else {
    			Router.push('/error')
    		}
    	})
		
	}

	
	useEffect(() => {
		// console.log(data)
		
	}, [])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`	
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data._id) {
				setIncomeNameGet(data.incomeRecords)
				// setIncomeNameGet(data.incomeRecords.map(item => {
				// 	return item.incomeName
				// }))
			} else {
				setIncomeNameGet([])
			}
			
		})

	}, [])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`	
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data._id) {
				setExpenseNameGet(data.expensesRecords)
				// setIncomeNameGet(data.incomeRecords.map(item => {
				// 	return item.incomeName
				// }))
			} else {
				setExpenseNameGet([])
			}
			
		})

	}, [])
	console.log(expenseNameGet)

	// useEffect(() => {
	// 	async function getServerSideProps() {
	// 		//fetch data from endpoint
	// 		const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`);
	// 		const data = await res.json()

	// 		//return the props
	// 		return {
	// 			props: {
	// 				data
	// 			}
	// 		}
	// 	}
	// }, [])

	const x = incomeNameGet.map(item => {
		return (
			<IncomeCard
				key={item._id}
				incomeProp={item}
			/>
		)
	})

	const y = expenseNameGet.map(item => {
		return (
			<ExpenseCard
				key={item._id}
				expenseProp={item}
			/>
		)
	})

	// .sort((a, b) => b.incomeDate > a.incomeDate)
	console.log(incomeNameGet)
	console.log(data)
	// console.log(sortedx)
	
// <IncomeCard incomeProp={incomeGet.incomeRecords}/>
	
	return (
		<React.Fragment>
			<Head>
				<title>Records</title>
			</Head>
		
			<Container>
				<Row>
					<Col>
						<Form onSubmit={(e) => addIncome(e)}>
							<Form.Group controlId="incomeGrp">
				                    <Form.Label>Add Income</Form.Label>
				                    <Form.Control type="addAmount" placeholder="Enter record name here" value={incomeName} onChange={e => setIncomeName(e.target.value)} required/>
				                    <Form.Control type="addAmount" placeholder="Add amount here" value={incomeAmount} onChange={e => setIncomeAmount(e.target.value)} required/>
				             </Form.Group>
				             <Button className="incomeBtn" variant="success" type="submit" id="incomeBtn">Income</Button>
				         </Form>
			         </Col>
			         <Col>
						<Form onSubmit={(d) => addExpense(d)}>
							<Form.Group controlId="expenseGrp">
				                    <Form.Label>Add Expense</Form.Label>
				                    <Form.Control type="addAmount" placeholder="Enter record name here" value={expenseName} onChange={d => setExpenseName(d.target.value)} required/>
				                    <Form.Control type="addAmount" placeholder="Add amount here" value={expenseAmount} onChange={d => setExpenseAmount(d.target.value)} required/>
				             </Form.Group>
				             <Button variant="danger" type="submit" id="expenseBtn">Expense</Button>
				         </Form>
			         </Col>
		         </Row>
	         </Container>
	         <br /><br />
	         <h2>Records</h2>
	         <Row>
	         	<Col xs={12} md={6}>
	         		{x}
	         	</Col>
	         	<Col xs={12} md={6}>
	         		{y}
	         	</Col>
	         </Row>
        </React.Fragment>
	)
}


// export async function getServerSideProps() {
// 	//fetch data from endpoint
// 	const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
// 		method: 'GET',
// 		headers: {
// 			'Authorization': `Bearer ${localStorage.getItem('token')}`,
//             	'Content-Type': 'application/json'
// 		}
// 	})
// 	const data = await res.json()

// 	//return the props
// 	return {
// 		props: {
// 			data
// 		}
// 	}
// }


//----------Bruno Artunes YT-----//
// export const getServerSideProps = async ctx => {
// 	const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`);
// 	const data = await res.json()

// 	return {
// 		props: {
// 			data
// 		}
// 	}
// }
//----------Bruno Artunes YT end-----//





// <React.Fragment>
// 			<Head>
// 				<title>Records</title>
// 			</Head>
		

// 			<Form onSubmit={(e) => addRecord(e)}>
// 				<Form.Group controlId="addIncomeOrExpense">
// 	                    <Form.Label>Add Record</Form.Label>
// 	                    <Form.Control type="addAmount" placeholder="Enter record name here" value={incomeName} onChange={e => setIncomeName(e.target.value)} required/>
// 	                    <Form.Control type="addAmount" placeholder="Add amount here" value={incomeAmount} onChange={e => setIncomeAmount(e.target.value)} required/>
// 	             </Form.Group>
// 	             <Button variant="success" type="submit" id="incomeBtn">Income</Button>
// 	             <Button variant="danger" type="submit" id="expenseBtn">Expense</Button>
// 	         </Form>
// 	         <h2>Records</h2>
//         </React.Fragment>