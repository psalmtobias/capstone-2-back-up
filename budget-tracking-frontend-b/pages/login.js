import { useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

import Router from 'next/router'
import Head from 'next/head'

import UserContext from '../UserContext'
import View from '../components/View'
import AppHelper from '../app-helper';


export default function login() {
    const { setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [tokenId, setTokenId] = useState(null)

    function authenticate(e) {

        // e.preventDefault()

    //     fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             email: email,
    //             password: password
    //         })
    //     })
    //     .then(res => res.json())
    //     .then(data => {
            
    //         if(data.accessToken){
                
    //             localStorage.setItem('token', data.accessToken);
                
    //             fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
    //                 headers: {
    //                     Authorization: `Bearer ${data.accessToken}`
    //                 } 
    //             })
    //             .then(res => res.json())
    //             .then(data => {
                    
    //                 setUser({
    //                     id: data._id
    //                 })
    //                 Router.push('/')
    //             })
    //         }else{
    //             Router.push('/')
    //         }
    //     })
    // }



    // function authenticate(e) {

        //prevent redirection via form submission
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist') {
                    Swal.fire(
                        'Authentication Failed',
                        'User does not exist.',
                        'error'
                    )
                } else if (data.error === 'incorrect-password') {
                    Swal.fire(
                        'Authentication Failed',
                        'Password is incorrect.',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure.',
                        'error'
                    )
                }  
            }
        })
    }



    const authenticateGoogleToken = (response) => {
        console.log(response)

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if(typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.',
                        'error'
                    ) 
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                } 
            }
        })
    }


    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }`}
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

            Router.push('/')
        })
    }



    return (
        <React.Fragment>
            <Head>
                <title>Login</title>
            </Head>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>


                <GoogleLogin
                    clientId="95819072606-3gta8np79bbp5c0dmehkag030jobje4r.apps.googleusercontent.com"
                    buttonText="Login"
                    onSuccess={ authenticateGoogleToken }
                    onFailure={ authenticateGoogleToken }
                    cookiePolicy={ 'single_host_origin' }
                    className="w-100 text-center d-flex justify-content-center"
                />

            </Form>
        </React.Fragment>
    )
}