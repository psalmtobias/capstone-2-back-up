import Head from 'next/head'
import dynamic from 'next/dynamic'

const DynamicComponent = dynamic(() => import('../components/StreetNavigation')) 

import StreetNavigation from '../components/StreetNavigation'

export default function travel(){
	return(
		<React.Fragment>
			<Head>
				<title>Book a ride</title>
			</Head>
			<DynamicComponent />
		</React.Fragment>
	)
}