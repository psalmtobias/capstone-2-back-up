import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
	const data = {
		title: "Budget Tracker",
		content: "Manage your money today."
	}
	
	return (
		<React.Fragment>
			<Head >
				<title>Budget Tracker</title>
			</Head>
			<Banner data={data} />
		</React.Fragment>
	)
}