import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights() {

	const [incomeTotal, setIncomeTotal] = useState([])
	const [expensesTotal, setExpensesTotal] = useState([])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.incomeRecords.length > 0 ){
				let monthlyIncome = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


				data.incomeRecords.forEach(incomeTotal => {
					// January = 0, February = 1 ......
					const index = moment(incomeTotal.date).month()

					monthlyIncome[index] += (incomeTotal.incomeAmount)
				})


				setIncomeTotal(monthlyIncome)
				// setDistances(monthlyDistance)
				// setAmounts(monthlyAmount)
				// setDurations(monthlyDuration)
				// console.log(monthlyDistance)
			}
		})
	}, [])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.expensesRecords.length > 0 ){
				let monthlyExpenses = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


				data.expensesRecords.forEach(expensesTotal => {
					// January = 0, February = 1 ......
					const index = moment(expensesTotal.date).month()

					monthlyExpenses[index] += (expensesTotal.expenseAmount)
				})


				setExpensesTotal(monthlyExpenses)
				// setDistances(monthlyDistance)
				// setAmounts(monthlyAmount)
				// setDurations(monthlyDuration)
				// console.log(monthlyDistance)
			}
		})
	}, [])


	return (
		<Tabs defaultActiveKey="incomeTotal" id="monthlyFigures">
			<Tab eventKey="incomeTotal" title="Income">
				<MonthlyChart figuresArray={incomeTotal} label="Monthly total in peso" />
			</Tab>
			<Tab eventKey="expensesTotal" title="Expenses">
				<MonthlyChart figuresArray={expensesTotal} label="Monthly total in peso" />
			</Tab>
		</Tabs>
	)
}