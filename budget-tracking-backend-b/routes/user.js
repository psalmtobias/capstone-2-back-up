const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

//for getting private user profile info
// eto yung version na may verify na kailangan pa ng auth
// may error to na "localstorage is not defined"
// kapag tinawag sa frontend, pero working sa postman
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.getDetails({ userId: user.id }).then(user => res.send(user))
})

// router.get('/details', (req, res) => {
//     const user = auth.decode(req.headers.authorization)
//     UserController.getDetails({ userId: user.id }).then(user => res.send(user))
// })






//for recording user travels
router.post('/travels', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        travel: {
            origin: {
                longitude: req.body.originLong,
                latitude: req.body.originLat
            },
            destination: {
                longitude: req.body.destinationLong,
                latitude: req.body.destinationLat
            },
            distance: req.body.distance,
            duration: req.body.duration,
            charge: {
                chargeId: req.body.chargeId,
                amount: req.body.amount
            }
        }
    }
    UserController.addTravel(params).then(result => res.send(result))
})





router.put('/', auth.verify, (req, res) => {
    UserController.update(req.body).then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})



router.post('/addCategory', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        category: {
            categoryName: req.body.categoryName,
            isExpense: req.body.isExpense
        }
    }
    UserController.addCategory(params).then(result => res.send(result))
})


// router.get('/incomeRecords', (req,res) => {
//     UserController.getAll().then(incomeRecords => res.send(incomeRecords))
// })

// router.get('/details', auth.verify, (req, res) => {
//     const user = auth.decode(req.headers.authorization);
//     UserController.get({ userId: user.id }).then(user => res.send(user))
// })


router.post('/addExpense', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        expensesRecord: {
            expenseName: req.body.expenseName,
            expenseAmount: req.body.expenseAmount,
            expenseDate: req.body.expenseDate
        }
    }
    UserController.addExpense(params).then(result => res.send(result))
})



router.post('/addIncome', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        incomeRecord: {
            incomeName: req.body.incomeName,
            incomeAmount: req.body.incomeAmount,
            incomeDate: req.body.incomeDate
        }
    }
    UserController.addIncome(params).then(result => res.send(result))
})


router.delete('/deleteIncome', auth.verify, (req, res) => {
    // const incomeRecordId = incomeRecord{ req.params.incomeRecordId }
    // UserController.deleteIncome({ incomeRecordId }).then(result => res.send(result))

    // const params = {
    //     userId: auth.decode(req.headers.authorization).id,
    //     incomeRecord: {
    //         incomeRecordId: req.body.incomeRecordId
    //     }
    // }
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        incomeRecord: {
            _id: req.body.incomeRecordId
        }
    }

    UserController.deleteIncome({params}).then(result => res.send(result))

})


module.exports = router