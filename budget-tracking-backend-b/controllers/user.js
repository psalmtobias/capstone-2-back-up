const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')

const { OAuth2Client } = require('google-auth-library')

const clientId = '95819072606-3gta8np79bbp5c0dmehkag030jobje4r.apps.googleusercontent.com'

//function to catch & handle errors
const errCatcher = err => console.log(err)

//function for finding duplicate emails
module.exports.emailExists = (params) => {
    //find a user document with matching email
    return User.find({ email: params.email })
    .then(result => {
        //if match found, return true
		return result.length > 0 ? true : false
    })
    .catch(errCatcher)
}

module.exports.register = (params) => {
    //instantiate a new user object
	let user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        mobileNo: params.mobileNo,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
        loginType: 'email',
        isAdmin: false
	})

    //save user object as a new document
    return user.save()
    .then((user, err) => {
        //if err generated, return false otherwise return true
		return (err) ? false : true
    })
    .catch(errCatcher)
}

// module.exports.login = (params) => {
//     //find a user with matching email
//     return User.findOne({ email: params.email })
//     .then(user => {
//         //if no match found, return false
// 		if (user === null) return false

//         //check if submitted password matches password on record
// 		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

//         //if matching password
// 		if (isPasswordMatched) {
//             //generate JWT
// 			return { accessToken: auth.createAccessToken(user.toObject()) }
// 		} else {
// 			return false
// 		}
//     })
//     .catch(errCatcher)
// }



module.exports.login = (params) => {
    return User.findOne({ email : params.email }).then(user => {
        if (user === null) {
            return { error: 'does-not-exist'};
        }
        if (user.loginType !== 'email') {
            return { error: 'login-type-error'}
        }

        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if (isPasswordMatched) {
            return { accessToken: auth.createAccessToken(user.toObject()) }
        } else {
            return { error: 'incorrect-password'};
        }
    })
}



module.exports.verifyGoogleTokenId = async (params) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({
        idToken: params,
        audience: clientId
    })

    if(data.payload.email_verified === true) {
        const user =  await User.findOne({ email: data.payload.email }).exec();

        if (user !== null) {
            if(user.loginType === "google") {
                return {
                    accessToken: auth.createAccessToken(user.toObject())
                }
            } else {
                return { error: "login-type-error" }
            }
        } else {
            const newUser = new User({
                email: data.payload.email,
                loginType: "google"
            })

            return newUser.save().then((user, err) => {
                return {
                    accessToken: auth.createAccessToken(user.toObject())
                }
            })
        }
    } else {
        return { error: "google-auth-error" }
    }
}



//function for getting details of a user based on a decoded token
module.exports.getDetails= (params) => {
    return User.findById(params.userId)
    .then(user => {
        //clear the password property of the user object for security
		user.password = undefined
		return user
    })
    .catch(errCatcher)
}

module.exports.addTravel = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.travels.push(params.travel)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
        })
        .catch(errCatcher)
    })
    .catch(errCatcher)
}





// module.exports.getAll = () => {
//     return User.find(incomeRecords: [{ isExpense: false }]).then(incomeRecords => incomeRecords)
// }


// module.exports.get = (params) => {
//     return User.findById( params.incomeRecordId ).then(incomeRecords => incomeRecords)
// }



module.exports.addCategory = ( params ) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.categories.push( params.category )
        .then((updateUser, err) => {
            return(err) ? false : true
        })
        console.log(err)
        console.log(params)
    })
    console.log(err)
}

module.exports.addExpense = ( params ) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.expensesRecords.push( params.expensesRecord )
        return user.save()
        .then((updateUser, err) => {
            return(err) ? false : true
        })
        console.log(err)
        console.log(params)
    })
    console.log(err)
}



module.exports.addIncome = ( params ) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.incomeRecords.push( params.incomeRecord )
        return user.save()
        .then((updateUser, err) => {
            return(err) ? false : true
        })
        console.log(err)
        console.log(params)
    })
    console.log(err)
}


module.exports.update = (params) => {

    // If gusto mo na prive lang ang palitan
    // if (params.name === "") {
    //  let course = Course.findById(params.courseId)
    //  updates.name = course.name
    // } else (params.description === ""){
    //  let course = Course.findById(params.courseId)
    //  updates.description = course.description
    // }

    const updates = {
        email: params.email,
        password: bcrypt.hashSync(params.password, 10)
    }



    return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
        return (err) ? false : true
    })
}




// module.exports.deleteIncome = (params) => {

    
//     User.findById(params.userId, (findErr, user) => {
//         if(findErr) {
//             return console.log(findErr)
//         }



//         user.incomeRecords.id(params.incomeRecordsId).remove()
//         user.save((saceErr, modifiedUser) => {
//             if (saveErr) {
//                 return console.log(saveeErr)
//             }
//             return res.status(200).json({
//                 message: `Income Records with id ${req.params.incomeRecordsId} deleted successfully`,
//                 data: modifiedUser.incomeRecord
//             })
//         })
//     })

//     // return User.findByIdAndDelete(params.incomeRecordId).then((doc, err) => {
//     //     return (err) ? false : true
//     // })

//     // const updates = { isActive: false }

//     // return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
//     //     return (err) ? false : true
//     // })
// }











module.exports.deleteIncome = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        // user.incomeRecords.remove( "_id": "{params.incomeRecordId}" )
        return user.save()
        .then((updateUser, err) => {
            return(err) ? false : true
        })

        console.log(user)
        console.log(params)
    })
    console.log(err)



    // x = User.find( params.userId )

    // return User.find( params.userId )
    // .then((user, err) => {
    //     if (err) return false

    //     console.log(params)
    //     console.log(user)
    //     User.findOne( params.incomeRecordId )
    //     .then((userA, err) => {
    //         console.log(userA)
    //     })
    // }) 
    

    

//-------------------------------------
    // const toBeDeleted = {
    //     userId: params.userId,
    //     expensesRecords: [{
    //         expensesRecordsId: params.incomeRecordId
    //     }]
    // }
        

    // User.find(toBeDeleted).remove()

    // return User.save().then((user,err) => {
    //     return (err) ? false : true
    // })


}






